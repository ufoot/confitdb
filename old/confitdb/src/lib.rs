//! [ConfitDB](https://gitlab.com/liberecofr/confitdb) is an experimental,
//! distributed, real-time database, giving full control on
//! conflict resolution, implemented in [Rust](https://www.rust-lang.org/).
//!
//! ![ConfitDB icon](https://gitlab.com/liberecofr/confitdb/raw/master/media/confitdb-256x256.jpg)
//!
//! Not even a prototype, just an idea. Under heavy construction.

mod access;
mod access_error;
mod cloud;
mod cloud_options;
mod conflict;
mod http_server;
mod key_value;
mod server_control;
mod server_error;
mod server_options;
mod server_status;
mod udp_server;

pub use access::*;
pub use access_error::*;
pub use cloud::*;
pub use cloud_options::*;
pub use confitul::CKey;
pub use confitul::Cache;
pub use confitul::VClock;
pub use conflict::*;
pub use server_error::*;
pub use server_options::*;
pub use server_status::*;
