use confitul::CKey;
use std::fmt;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum AccessError {
    StillConflicts(usize),
    CantDecode(String),
    CantEncode(String),
    KeyMismatch((String, String)),
    EmptyKey(CKey),
}

impl From<&AccessError> for String {
    fn from(err: &AccessError) -> String {
        match err {
            AccessError::StillConflicts(n) => {
                format!("there are still {} conflicts, resolve them first", n)
            }
            AccessError::CantDecode(msg) => {
                format!("can not decode content, upstream error: {}", msg)
            }
            AccessError::CantEncode(msg) => {
                format!("can not encode content, upstream error: {}", msg)
            }
            AccessError::KeyMismatch(keys) => {
                format!("key mismatch: {} != {}", keys.0, keys.1)
            }
            AccessError::EmptyKey(key) => {
                format!("empty key for {}", key)
            }
        }
    }
}

impl fmt::Display for AccessError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", String::from(self))
    }
}
