use confitul::{ClusterOptions, ConfigCheck, ConfigCheckError};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CloudOptions {
    pub cluster: ClusterOptions,
}

impl CloudOptions {
    pub fn new(cluster_options: Option<ClusterOptions>) -> CloudOptions {
        let real_cluster_options = cluster_options.unwrap_or(ClusterOptions::default());
        let mut cloud_options = Self::default();
        cloud_options.cluster = real_cluster_options;
        cloud_options
    }
}

impl ConfigCheck for CloudOptions {
    fn check(&self) -> Result<CloudOptions, ConfigCheckError> {
        self.cluster.check()?;
        Ok(self.clone())
    }
}

impl std::default::Default for CloudOptions {
    fn default() -> CloudOptions {
        CloudOptions {
            cluster: ClusterOptions::default(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::CloudOptions;
    use gethostname::gethostname;
    use serde_json;

    #[test]
    fn test_cloud_options_serde_json() {
        let options = CloudOptions::new(None);
        let serialized = serde_json::to_string(&options).unwrap();
        assert_eq!(
            format!(
                "{{\"cluster\":{{\"size\":10,\"local_host\":{{\"name\":\"{}\",\"description\":\"\",\"urls\":[]}}}}}}",
                gethostname().to_str().unwrap()
            ),
            serialized
        );
    }
}
