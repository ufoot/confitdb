use crate::access_error::AccessError;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::io::Cursor;

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub(crate) struct KeyValue {
    pub(crate) key: String,
    #[serde(with = "serde_bytes")]
    pub(crate) value: Vec<u8>,
}

impl KeyValue {
    pub(crate) fn serialize_value<V>(value: &Option<V>) -> Result<Vec<u8>, AccessError>
    where
        V: Serialize,
    {
        match rmp_serde::to_vec(&value) {
            Ok(v) => Ok(v),
            Err(e) => Err(AccessError::CantEncode(format!("{:?}", e))),
        }
    }

    pub(crate) fn deserialize_value<V>(vec: &Vec<u8>) -> Result<Option<V>, AccessError>
    where
        V: DeserializeOwned,
    {
        let reader = Cursor::new(vec);
        match rmp_serde::from_read(reader) {
            Ok(v) => Ok(v),
            Err(e) => Err(AccessError::CantDecode(format!("{:?}", e))),
        }
    }
}
