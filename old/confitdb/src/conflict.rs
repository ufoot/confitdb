use crate::access_error::AccessError;
use crate::key_value::KeyValue;
use confitul;
use confitul::{CKey, VClock};
use rmp_serde;
use serde::de::DeserializeOwned;
use serde::Serialize;
use std::io::Cursor;

pub struct Conflict<V>
where
    V: Serialize + DeserializeOwned,
{
    pub(crate) key: String,
    pub(crate) left: Option<V>,
    pub(crate) right: Option<V>,
    pub(crate) upstream_key: CKey,
    pub(crate) upstream_version: VClock<u64>,
}

impl<V> Conflict<V>
where
    V: Serialize + DeserializeOwned,
{
    pub(crate) fn try_new(
        upstream: &confitul::Conflict<u64, KeyValue>,
    ) -> Result<Self, AccessError> {
        let mut key: String = "".to_string();
        let left = match &upstream.left.value {
            Some(upstream_left_value) => {
                let reader = Cursor::new(&upstream_left_value.value);
                match rmp_serde::from_read(reader) {
                    Ok(v) => {
                        key = upstream_left_value.key.clone();
                        v
                    }
                    Err(e) => return Err(AccessError::CantDecode(format!("{:?}", e))),
                }
            }
            None => None,
        };
        let right = match &upstream.right.value {
            Some(upstream_right_value) => {
                let reader = Cursor::new(&upstream_right_value.value);
                match rmp_serde::from_read(reader) {
                    Ok(v) => {
                        if key.len() > 0
                            && upstream_right_value.key.len() > 0
                            && key != upstream_right_value.key
                        {
                            return Err(AccessError::KeyMismatch((
                                key,
                                upstream_right_value.key.clone(),
                            )));
                        }
                        v
                    }
                    Err(e) => return Err(AccessError::CantDecode(format!("{:?}", e))),
                }
            }
            None => None,
        };
        if key.len() == 0 {
            return Err(AccessError::EmptyKey(upstream.key));
        }
        let upstream_key = upstream.key;
        let upstream_version = upstream.left.version.merge(&upstream.right.version);
        Ok(Conflict {
            key,
            left,
            right,
            upstream_key,
            upstream_version,
        })
    }

    pub fn key(&self) -> &String {
        &self.key
    }

    pub fn left(&self) -> Option<&V> {
        match &self.left {
            Some(left) => Some(left),
            None => None,
        }
    }

    pub fn right(&self) -> Option<&V> {
        match &self.right {
            Some(right) => Some(right),
            None => None,
        }
    }
}
