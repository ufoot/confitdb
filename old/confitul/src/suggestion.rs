use crate::versioned::Versioned;
use ckey::CKey;
use std::hash::Hash;

#[derive(Debug, Clone)]
pub struct Suggestion<C, V>
where
    C: Eq + Hash + Clone,
    V: Clone,
{
    pub key: CKey,
    pub versioned: Versioned<C, V>,
}
