use crate::host_id::HostId;
use crate::host_sig::HostSig;
use serde::{Deserialize, Serialize};
use std::fmt::Formatter;
use url::Url;

/// HostInfo corresponds to an aggregation of nodes, running within a same program
/// instance. Think of them as "physical", macro entities, as opposed to nodes
/// which are much more virtual and small. A given host will typically have
/// several nodes to power, all of them going through the same addresses (http etc.).
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct HostInfo {
    /// ID used by the host. This happens to be the public key as well,
    /// when it comes to signing messages.
    pub id: HostId,
    /// Name of the host, a free-form test, by default the hostname.
    pub name: String,
    /// Description of the host, a free-form text.
    pub description: String,
    /// URLs of the host, how to connect to it.
    pub urls: Vec<Url>,
    /// Signature of the host info.
    pub sig: HostSig,
}

impl HostInfo {
    pub fn content_to_verify(&self) -> Vec<u8> {
        Vec::from(format!("{}", &self))
    }
}

impl std::fmt::Display for HostInfo {
    /// Pretty-print a host.
    ///
    /// # Examples
    /// ```
    /// use confitul::HostInfo;
    /// use confitul::HostId;
    /// use confitul::HostSig;
    /// use ed25519_dalek::Keypair;
    /// use rand07::rngs::OsRng;
    /// use url::Url;
    /// use std::convert::TryFrom;
    ///
    /// let mut csprng = OsRng {};
    /// let keypair: Keypair = Keypair::generate(&mut csprng);
    ///
    /// let h = HostInfo {
    ///     id: HostId::new(&keypair),
    ///     name: String::from("computer"),
    ///     description: String::from("test"),
    ///     urls: vec![Url::parse("http://localhost").unwrap()],
    ///     sig: HostSig::default(),
    /// };
    /// assert_eq!("{\"id\":\"0x", &format!("{}", h)[0..9]);
    /// assert_eq!("\",\"name\":\"computer\",\"description\":\"test\",\"urls\":[\"http://localhost/\"]}", &format!("{}", h)[18..88]);
    /// ```
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{{\"id\":\"{}\",\"name\":\"{}\",\"description\":\"{}\",\"urls\":[",
            self.id, self.name, self.description,
        )?;
        let mut require_comma = false;
        for url in self.urls.iter() {
            if require_comma {
                write!(f, ",")?;
                require_comma = true;
            }
            write!(f, "\"{}\"", url.as_str())?;
        }
        write!(f, "]}}")
    }
}
