use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Constant<T> {
    pub value: T,
}

impl<T> Constant<T> {
    fn new(value: T) -> Self {
        Constant { value }
    }
}
