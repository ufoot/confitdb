use crate::versioned::Versioned;
use ckey::CKey;
use std::hash::Hash;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Conflict<C, V>
where
    C: Eq + Hash + Clone,
    V: Clone,
{
    pub key: CKey,
    pub left: Versioned<C, V>,
    pub right: Versioned<C, V>,
}
