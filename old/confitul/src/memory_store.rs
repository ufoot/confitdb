use crate::conflict::Conflict;
use crate::disk_store::DiskStore;
use crate::suggestion::Suggestion;
use crate::versioned::{Versioned, VersionedStatus};
use ckey::CKey;
use hashlru::Cache;
use serde::de::DeserializeOwned;
use serde::Serialize;
use squeue::{Drain, SyncQueue};
use std::cmp::Ordering::{Equal, Greater, Less};
use std::hash::Hash;
use std::iter::Iterator;
use std::sync::{Arc, RwLock};
use vclock::VClock64;

#[derive(Debug)]
pub struct MemoryStore<C, V, T>
where
    C: Eq + Hash + Clone + Serialize + DeserializeOwned,
    V: Eq + Clone + Serialize + DeserializeOwned,
    T: Clone + Serialize + DeserializeOwned,
{
    items: Arc<RwLock<Cache<CKey, Versioned<C, V>>>>,
    consts: Arc<RwLock<Cache<CKey, T>>>,
    items_remote_updates: SyncQueue<CKey>,
    consts_remote_updates: SyncQueue<CKey>,
    items_conflicts: SyncQueue<Conflict<C, V>>,
    suggestions: SyncQueue<Suggestion<C, V>>,
    suggestion_mode: Arc<RwLock<SuggestionMode>>,
    disk_store: DiskStore<C, V, T>,
}

const MEMORY_STORE_ITEMS_SIZE: usize = 10_000; // [TODO] make this a param
const MEMORY_STORE_CONSTS_SIZE: usize = 10_000; // [TODO] make this a param
const MEMORY_STORE_ITEMS_REMOTE_UPDATES_SIZE: usize = 10_000; // [TODO] make this a param
const MEMORY_STORE_CONSTS_REMOTE_UPDATES_SIZE: usize = 10_000; // [TODO] make this a param
const MEMORY_STORE_ITEMS_CONFLICTS_SIZE: usize = 10_000; // [TODO] make this a param
const MEMORY_STORE_SUGGESTIONS_SIZE: usize = 10_000; // [TODO] make this a param

#[derive(Debug, Clone, Copy)]
pub enum SuggestionMode {
    Assign,
    Queue,
    Drop,
}

impl<C, V, T> MemoryStore<C, V, T>
where
    C: Eq + Hash + Clone + Serialize + DeserializeOwned,
    V: Eq + Clone + Serialize + DeserializeOwned,
    T: Clone + Serialize + DeserializeOwned,
{
    pub fn new() -> Self {
        MemoryStore::<C, V, T> {
            items: Arc::new(RwLock::new(Cache::new(MEMORY_STORE_ITEMS_SIZE))),
            consts: Arc::new(RwLock::new(Cache::new(MEMORY_STORE_CONSTS_SIZE))),
            items_remote_updates: SyncQueue::new(MEMORY_STORE_ITEMS_REMOTE_UPDATES_SIZE),
            consts_remote_updates: SyncQueue::new(MEMORY_STORE_CONSTS_REMOTE_UPDATES_SIZE),
            items_conflicts: SyncQueue::new(MEMORY_STORE_ITEMS_CONFLICTS_SIZE),
            suggestions: SyncQueue::new(MEMORY_STORE_SUGGESTIONS_SIZE),
            suggestion_mode: Arc::new(RwLock::new(SuggestionMode::Assign)),
            disk_store: DiskStore::new(),
        }
    }

    pub fn items_len(&self) -> usize {
        self.items.read().unwrap().len()
    }

    pub fn items_capacity(&self) -> usize {
        self.items.read().unwrap().capacity()
    }

    pub fn resize_items(&self, size: usize) -> usize {
        self.items.write().unwrap().resize(size)
    }

    pub fn items_conflicts_len(&self) -> usize {
        self.items_conflicts.len()
    }

    pub fn items_conflicts_capacity(&self) -> usize {
        self.items_conflicts.capacity()
    }

    pub fn resize_items_conflicts(&self, size: usize) -> usize {
        self.items_conflicts.resize(size)
    }

    pub fn set_suggestion_mode(&self, mode: SuggestionMode) {
        *self.suggestion_mode.write().unwrap() = mode;
    }

    pub fn get_suggestion_mode(&self) -> SuggestionMode {
        *self.suggestion_mode.read().unwrap()
    }

    pub fn suggestions_len(&self) -> usize {
        self.items.read().unwrap().len()
    }

    pub fn suggestions_capacity(&self) -> usize {
        self.suggestions.capacity()
    }

    pub fn resize_suggestions(&self, size: usize) -> usize {
        self.suggestions.resize(size)
    }

    pub fn assign_versioned(
        &self,
        key: CKey,
        versioned: Versioned<C, V>,
    ) -> Option<Conflict<C, V>> {
        let changed = {
            let mut changed = false;
            let mut items = self.items.write().unwrap();
            match {
                match items.get(&key) {
                    Some(old) => Some(old),
                    None => match self.disk_store.items_store_.get(&key) {
                        // Item was not in hot memory but stored in disk -> let's store
                        // it back to our live cache and keep going.
                        Some(old) => {
                            items.set(&key, old);
                            Some(old)
                        }
                        None => None,
                    },
                }
            } {
                Some(old) => {
                    match versioned.version.partial_cmp(&old.version) {
                        Some(Greater) => {
                            // This is more recent than what we have -> write it.
                            items.insert(key, versioned);
                            changed = true;
                        }
                        Some(Equal) | Some(Less) => {
                            // Nothing to do here, this is an old version,
                            // so what we have is more recent, keep it.
                        }
                        None => {
                            if old.value == versioned.value {
                                // there is a version conflict but the data is the same,
                                // auto-resolve and just put a version that acknowledges
                                // both "changes". Can happen if two different authors
                                // come to the same content independently.
                                let resolved = Versioned {
                                    version: (&old.version).clone().combine(&versioned.version),
                                    value: versioned.value,
                                };
                                items.insert(key, resolved);
                                changed = true;
                            } else {
                                // true conflict, do not modify store and return a conflict.
                                // In reality, the store has been updated because we did
                                // a get() so the order of keys has been atlered in the LRU,
                                // but the values have not been altered.
                                return Some(Conflict {
                                    key,
                                    left: old.clone(),
                                    right: versioned,
                                });
                            }
                        }
                    };
                }
                None => {
                    // Initial write, there was no version for it yet.
                    items.insert(key, versioned);
                    changed = true;
                }
            }
            changed
        };
        if changed {
            self.disk_store.items_store().put(&key, versioned)
        }
        None
    }

    pub fn assign_value(
        &self,
        key: CKey,
        version: VClock64<C>,
        value: V,
    ) -> Option<Conflict<C, V>> {
        self.assign_versioned(
            key,
            Versioned {
                version,
                value: Some(value),
            },
        )
    }

    pub fn assign_kill(&self, key: CKey, version: VClock64<C>) -> Option<Conflict<C, V>> {
        self.assign_versioned(
            key,
            Versioned {
                value: None,
                version,
            },
        )
    }

    pub fn suggest_versioned(&self, key: CKey, versioned: Versioned<C, V>) {
        let suggestion_mode = self.suggestion_mode.read().unwrap();
        match *suggestion_mode {
            SuggestionMode::Assign => {
                if let Some(conflict) = self.assign_versioned(key, versioned) {
                    self.push_conflict(conflict);
                }
            }
            SuggestionMode::Queue => {
                self.suggestions.push(Suggestion { key, versioned });
            }
            SuggestionMode::Drop => (),
        }
        drop(suggestion_mode);
    }

    pub fn suggest_value(&self, key: CKey, version: VClock64<C>, value: V) {
        self.suggest_versioned(
            key,
            Versioned {
                version,
                value: Some(value),
            },
        );
    }

    pub fn suggest_kill(&self, key: CKey, version: VClock64<C>) {
        self.suggest_versioned(
            key,
            Versioned {
                value: None,
                version,
            },
        );
    }

    pub fn squash_versioned(&self, key: CKey, versioned: Versioned<C, V>, author: C) {
        {
            let mut items = self.items.write().unwrap();
            let mut version = match items.peek(&key) {
                Some(old) => (&old.version).clone().combine(&versioned.version),
                None => versioned.version,
            };
            // This is a key call -> when squashing data, we need to keep track
            // of who did it. Each author has a unique ID and as they squash data,
            // they leave a trace of what they did, therefore triggering possible
            // later conflicts. If one is using the memory store alone, there is
            // never such a conflict, one can squash forever.
            version.incr(&author);
            let versioned = Versioned {
                version,
                value: versioned.value,
            };
            self.disk_store.items_store_put(&key, &versioned);
            items.insert(key, versioned);
        }
        self.items_remote_updates.push(key);
    }

    pub fn squash_value(&self, key: CKey, version: VClock64<C>, value: V, author: C) {
        self.squash_versioned(
            key,
            Versioned {
                version,
                value: Some(value),
            },
            author,
        )
    }

    pub fn squash_kill(&self, key: CKey, version: VClock64<C>, author: C) {
        self.squash_versioned(
            key,
            Versioned {
                value: None,
                version,
            },
            author,
        )
    }

    pub fn get_versioned(&self, key: &CKey) -> Option<Versioned<C, V>> {
        let mut items = self.items.write().unwrap();

        match items.get(key) {
            // We clone the items, this is the whole point of
            // this store, it copies the items so that you
            // can freely use it outside the store.
            //
            // To modify it you *HAVE* to go through the conflict
            // system so a direct access makes no sense.
            Some(v) => Some(v.clone()),
            None => None,
        }
    }

    pub fn get_value(&self, key: &CKey) -> Option<V> {
        let mut items = self.items.write().unwrap();

        match items.get(key) {
            // We clone the items, this is the whole point of
            // this store, it copies the items so that you
            // can freely use it outside the store.
            //
            // To modify it you *HAVE* to go through the conflict
            // system so a direct access makes no sense.
            Some(v) => match &v.value {
                Some(value) => Some(value.clone()),
                None => None,
            },
            None => None,
        }
    }

    pub fn get_status(&self, key: &CKey) -> VersionedStatus {
        let items = self.items.read().unwrap();

        match items.peek(key) {
            Some(v) => {
                if v.value.is_none() {
                    VersionedStatus::WasKilled
                } else {
                    VersionedStatus::HasValue
                }
            }
            None => VersionedStatus::NotExist,
        }
    }

    pub fn has_versioned(&self, key: &CKey) -> bool {
        self.items.read().unwrap().contains_key(&key)
    }

    pub fn has_value(&self, key: &CKey) -> bool {
        let items = self.items.read().unwrap();

        match items.peek(key) {
            Some(v) => !v.value.is_none(),
            None => false,
        }
    }

    pub fn is_killed(&self, key: &CKey) -> bool {
        let items = self.items.read().unwrap();

        match items.peek(key) {
            Some(v) => v.value.is_none(),
            None => false,
        }
    }

    pub fn push_conflict(&self, conflict: Conflict<C, V>) {
        self.items_conflicts.push(conflict);
    }

    pub fn pop_conflict(&self) -> Option<Conflict<C, V>> {
        self.items_conflicts.pop()
    }

    pub fn iter_items(&self) -> MemoryStoreItemsIterator<C, V> {
        MemoryStoreItemsIterator::new(self.items.clone())
    }

    pub fn drain_items_conflicts(&self) -> Drain<Conflict<C, V>> {
        self.items_conflicts.drain()
    }

    pub fn drain_suggestions(&self) -> Drain<Suggestion<C, V>> {
        self.suggestions.drain()
    }

    pub fn freeze(&self) -> Drain<Conflict<C, V>> {
        let items_conflicts = self.drain_items_conflicts();

        let mut suggestion_mode = self.suggestion_mode.write().unwrap();
        *suggestion_mode = SuggestionMode::Queue;
        drop(suggestion_mode);

        items_conflicts
    }

    pub fn unfreeze(&self) -> usize {
        let suggestions = self.drain_suggestions();
        let suggestions_count = suggestions
            .map(|suggestion| self.assign_versioned(suggestion.key, suggestion.versioned))
            .count();

        let mut suggestion_mode = self.suggestion_mode.write().unwrap();
        *suggestion_mode = SuggestionMode::Assign;
        drop(suggestion_mode);

        suggestions_count
    }
}

impl<C, V, T> MemoryStore<C, V, T>
where
    C: Eq + Hash + Clone + Serialize + DeserializeOwned,
    V: Eq + Clone + Serialize + DeserializeOwned,
    T: Clone + Serialize + DeserializeOwned,
{
    pub fn get_const(&self, key: &CKey) -> Option<T> {
        match self.consts.write().unwrap().get(key) {
            Some(value) => Some(value.clone()),
            None => match self.consts_store.get().unwrap() {
                Some(value) => {
                    self.consts.write().unwrap().push(key.clone(), value);
                    Some(value)
                }
                None => None,
            },
        }
    }

    pub fn set_const(&self, value: T) -> CKey {
        let key = CKey::serial(&value);
        let changed = { self.consts.write().unwrap().insert(key, value).is_none() };
        if changed {
            self.consts_remote_updates.push(key);
        }
        self.disk_store.consts_store().put(&key, &value);
        key
    }
}

pub struct MemoryStoreItemsIterator<C, V>
where
    C: Eq + Hash + Clone,
    V: Clone,
{
    pos: usize,
    keys: Vec<CKey>,
    items: Arc<RwLock<Cache<CKey, Versioned<C, V>>>>,
}

impl<C, V> MemoryStoreItemsIterator<C, V>
where
    C: Eq + Hash + Clone,
    V: Clone,
{
    fn new(items: Arc<RwLock<Cache<CKey, Versioned<C, V>>>>) -> Self {
        // Build a list of all keys to iterate on, we do not know
        // what will happen to the store afterwards...
        let keys: Vec<CKey> = items.read().unwrap().iter().map(|x| *x.0).collect();
        Self {
            pos: 0,
            keys,
            items,
        }
    }
}

impl<C, V> Iterator for MemoryStoreItemsIterator<C, V>
where
    C: Eq + Hash + Clone,
    V: Clone,
{
    type Item = Versioned<C, V>;

    fn next(&mut self) -> Option<Versioned<C, V>> {
        while self.pos < self.keys.len() {
            match self.items.read().unwrap().peek(&self.keys[self.pos]) {
                Some(versioned) => {
                    if !versioned.value.is_none() {
                        return Some(versioned.clone());
                    }
                }
                None => (),
            }
            self.pos += 1;
        }
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_assign_value() {
        let ms: MemoryStore<u8, String, ()> = MemoryStore::new();

        let c1 = VClock64::new(1);
        let k1 = CKey::from(0.1);
        let c2 = VClock64::new(2);
        let k2 = CKey::from(0.2);
        assert_eq!(None, ms.assign_value(k1, c1.clone(), "abc".to_string()));
        assert_eq!(None, ms.assign_value(k2, c2.clone(), "def".to_string()));
        let conflict = ms
            .assign_value(k1, c2.clone(), "ooops".to_string())
            .unwrap();
        assert_eq!(
            Conflict {
                key: k1,
                left: Versioned {
                    version: c1.clone(),
                    value: Some("abc".to_string())
                },
                right: Versioned {
                    version: c2.clone(),
                    value: Some("ooops".to_string())
                }
            },
            conflict
        );
    }
}
