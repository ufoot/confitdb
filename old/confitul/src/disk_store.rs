use crate::ver_or_cst::VerOrCst;
use crate::versioned::Versioned;
use ckey::CKey;
use menhirkv::Store;
use serde::de::DeserializeOwned;
use serde::Serialize;
use std::hash::Hash;

#[derive(Debug)]
pub struct DiskStore<C, V, T>
where
    C: Eq + Hash + Clone + Serialize + DeserializeOwned,
    V: Serialize + DeserializeOwned,
    T: Serialize + DeserializeOwned,
{
    store: Store<CKey, VerOrCst<C, V, T>>,
}

const DISK_STORE_CF_ITEMS: &str = "items";
const DISK_STORE_CF_CONSTS: &str = "consts";
const DISK_STORE_SIZE: usize = 100_000; // [TODO] make this a param

impl<C, V, T> DiskStore<C, V, T>
where
    C: Eq + Hash + Clone + Serialize + DeserializeOwned,
    V: Serialize + DeserializeOwned,
    T: Serialize + DeserializeOwned,
{
    pub fn new() -> Self {
        DiskStore {
            store: Store::open_cf_temporary(
                &[DISK_STORE_CF_ITEMS, DISK_STORE_CF_CONSTS],
                DISK_STORE_SIZE,
            )
            .unwrap(),
        }
    }

    pub fn capacity(&self) -> usize {
        self.store.capacity()
    }

    pub fn items_store(&self) -> Store<CKey, VerOrCst<C, V, T>> {
        self.store.cf(DISK_STORE_CF_ITEMS).unwrap()
    }

    pub fn consts_store(&self) -> Store<CKey, VerOrCst<C, V, T>> {
        self.store.cf(DISK_STORE_CF_CONSTS).unwrap()
    }

    pub fn items_store_get(&self, key: &CKey) -> Option<Versioned<C, V>> {
        match self.items_store().get(key) {
            Some(v) => Some(Versioned::try_from(v).unwrap()),
            None => None,
        }
    }

    pub fn items_store_put(&self, key: &CKey, v: &Versioned<C, V>) {
        self.items_store().put(key, &VerOrCst::from(v));
    }
}
