use crate::constant::Constant;
use crate::error;
use crate::versioned::Versioned;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::convert;
use std::hash::Hash;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VerOrCst<C, V, T>
where
    C: Eq + Hash + Clone,
{
    versioned: Option<Versioned<C, V>>,
    constant: Option<Constant<T>>,
}

impl<C, V, T> From<Versioned<C, V>> for VerOrCst<C, V, T>
where
    C: Eq + Hash + Clone,
{
    fn from(versioned: Versioned<C, V>) -> VerOrCst<C, V, T> {
        VerOrCst {
            versioned: Some(versioned),
            constant: None,
        }
    }
}

impl<C, V, T> From<Constant<T>> for VerOrCst<C, V, T>
where
    C: Eq + Hash + Clone,
{
    fn from(constant: Constant<T>) -> VerOrCst<C, V, T> {
        VerOrCst {
            versioned: None,
            constant: Some(constant),
        }
    }
}

impl<C, V, T> convert::TryFrom<VerOrCst<C, V, T>> for Versioned<C, V>
where
    C: Eq + Hash + Clone,
{
    type Error = error::Error;

    fn try_from(v_or_c: VerOrCst<C, V, T>) -> Result<Versioned<C, V>, error::Error> {
        match v_or_c.versioned {
            Some(versioned) => Ok(versioned),
            None => Err(error::Error::invalid_data("no versioned data")),
        }
    }
}

impl<C, V, T> convert::TryFrom<VerOrCst<C, V, T>> for Constant<T>
where
    C: Eq + Hash + Clone,
{
    type Error = error::Error;

    fn try_from(v_or_c: VerOrCst<C, V, T>) -> Result<Constant<T>, error::Error> {
        match v_or_c.constant {
            Some(constant) => Ok(constant),
            None => Err(error::Error::invalid_data("no constant data")),
        }
    }
}
