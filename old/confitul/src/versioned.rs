use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::hash::Hash;
use vclock::VClock64;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Versioned<C, V>
where
    C: Eq + Hash + Clone,
{
    pub version: VClock64<C>,
    pub value: Option<V>,
}

#[derive(Clone, Copy)]
pub enum VersionedStatus {
    HasValue,
    WasKilled,
    NotExist,
}

impl<C, V> Versioned<C, V>
where
    C: Eq + Hash + Clone + Serialize,
{
    fn new(version: VClock64<C>, value: Option<V>) -> Self {
        Versioned { version, value }
    }
}
