use ckey::CKey;
use std::fmt::Formatter;

#[derive(Default, Clone, Copy, PartialEq, Eq)]
pub struct Range {
    pub begin: CKey,
    pub end: CKey,
}

impl Range {
    pub fn new(begin: CKey, end: CKey) -> Range {
        Range { begin, end }
    }
}

impl std::fmt::Display for Range {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{},{}]", self.begin, self.end)
    }
}
