use std::fmt;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ServerStatus {
    Stopped,
    Bootstrapping,
    Running,
    Terminating,
}

impl From<ServerStatus> for String {
    fn from(s: ServerStatus) -> String {
        let txt: &str = s.into();
        txt.to_string()
    }
}

impl From<ServerStatus> for &str {
    fn from(s: ServerStatus) -> &'static str {
        match s {
            ServerStatus::Stopped => "stopped",
            ServerStatus::Bootstrapping => "bootstrapping",
            ServerStatus::Running => "running",
            ServerStatus::Terminating => "terminating",
        }
    }
}

impl fmt::Display for ServerStatus {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let txt: &str = (*self).into();
        write!(f, "{}", txt)
    }
}
