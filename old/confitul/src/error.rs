use hyper;
use menhirkv;
use std::fmt;
use std::fmt::{Display, Formatter};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, Clone)]
pub enum Error {
    /// Storage related error, could be DB, config, I/O.
    Storage(String),
    /// Network related error, can't connect, bad DNS, etc.
    Network(String),
    /// Invalid data, typically a serialization problem, or invalid input.
    InvalidData(String),
    /// Unexpected code path are behavior, please report issue on
    /// <https://gitlab.com/liberecofr/confitdb/-/issues>.
    ReportBug(String),
    /// Feature is not implemented yet.
    NotImplemented,
}

/// URL to report bugs.
///
/// This is used internally to provide context when something unexpected happens,
/// so that users can find find out which piece of software fails,
/// and how to contact author(s).
///
/// Alternatively, send a direct email to <ufoot@ufoot.org>.
pub const BUG_REPORT_URL: &str = "https://gitlab.com/liberecofr/confitdb/-/issues";

impl Error {
    pub(crate) fn storage(why: &str) -> Error {
        Error::Storage(why.to_string())
    }

    pub(crate) fn network(why: &str) -> Error {
        Error::Network(why.to_string())
    }

    pub(crate) fn invalid_data(why: &str) -> Error {
        Error::InvalidData(why.to_string())
    }

    pub(crate) fn report_bug(why: &str) -> Error {
        Error::ReportBug(why.to_string())
    }

    pub(crate) fn not_implemented() -> Error {
        Error::NotImplemented
    }
}

/// Convert from a MenhirKV error.
impl From<menhirkv::Error> for Error {
    fn from(e: menhirkv::Error) -> Error {
        match e {
            // Anything from menhirkv is considered a database issue.
            menhirkv::Error::Db(_) => Error::storage(&format!("[menhirkv] {}", e)),
            menhirkv::Error::InvalidData(_) => Error::storage(&format!("[menhirkv] {}", e)),
            // Do not chain those, fail now.
            menhirkv::Error::ReportBug(_) => panic!("[menhirkv] {}", e),
            menhirkv::Error::NotImplemented => panic!("[menhirkv] {}", e),
        }
    }
}

/// Convert from a Hyper error.
impl From<hyper::Error> for Error {
    fn from(e: hyper::Error) -> Error {
        Error::network(&format!("[hyper] {}", e))
    }
}

/// Convert from unit.
///
/// This returns "not implemented", it is just here to
/// make sure all the chain serving the "not implemented"
/// series of errors is here, and public API remains
/// stable.
impl From<()> for Error {
    fn from(_: ()) -> Error {
        Error::not_implemented()
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::result::Result<(), fmt::Error> {
        match self {
            Error::Storage(e) => write!(f, "storage error: {}", e),
            Error::Network(e) => write!(f, "network error: {}", e),
            Error::InvalidData(e) => write!(f, "invalid data: {}", e),
            Error::ReportBug(e) => write!(
                f,
                "unexpected bug, please report issue on <{}>: {}",
                BUG_REPORT_URL, e
            ),
            Error::NotImplemented => write!(f, "not implemented"),
        }
    }
}
