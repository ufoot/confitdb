use std::fmt::Formatter;
use url::ParseError;

#[derive(Debug, PartialEq, Eq)]
pub struct EmptyFieldError {
    pub field: String,
}

impl EmptyFieldError {
    pub fn new(field: &str) -> EmptyFieldError {
        EmptyFieldError {
            field: String::from(field),
        }
    }
}

impl std::fmt::Display for EmptyFieldError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "field {} is empty", self.field)
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct BadSizeError {
    pub field: String,
    pub value: usize,
    pub min: usize,
    pub max: usize,
}

impl BadSizeError {
    pub fn new(field: &str, value: usize, min: usize, max: usize) -> BadSizeError {
        BadSizeError {
            field: String::from(field),
            value,
            min,
            max,
        }
    }

    pub fn check(
        field: &str,
        value: usize,
        min: usize,
        max: usize,
    ) -> Result<(), ConfigCheckError> {
        if value < min {
            return Err(ConfigCheckError::BadSize(BadSizeError::new(
                field, value, min, max,
            )));
        }
        if value > max {
            return Err(ConfigCheckError::BadSize(BadSizeError::new(
                field, value, min, max,
            )));
        }
        Ok(())
    }
}

impl std::fmt::Display for BadSizeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "in field {} size {} should be between {} and {}",
            self.field, self.value, self.min, self.max
        )
    }
}

pub enum ConfigCheckError {
    Parse(ParseError),
    Empty(EmptyFieldError),
    BadSize(BadSizeError),
}

pub trait ConfigCheck
where
    Self: Sized,
{
    fn check(&self) -> Result<Self, ConfigCheckError>;
}
