use crate::host_id::HostId;
use crate::host_info::HostInfo;
use crate::host_sig::HostSig;
use signature;
use url::Url;

pub trait Host {
    /// Informations about the host.
    ///
    /// This returns the information that is common to all hosts,
    /// and is easy to marshal to dump/restore.
    fn info(&self) -> &HostInfo;

    /// ID of the host.
    ///
    /// Helper method to access directly the ID of the host.
    ///
    /// # Examples
    /// ```
    /// use confitul::LocalHost;
    /// use confitul::Host;
    ///
    /// let local_host = LocalHost::new(None).unwrap();
    /// let info = local_host.info();
    /// assert_eq!(info.id, local_host.id());
    /// ```
    fn id(&self) -> HostId {
        self.info().id
    }

    /// Name of the host.
    ///
    /// Helper method to access directly the name of the host.
    ///
    /// # Examples
    /// ```
    /// use confitul::LocalHost;
    /// use confitul::Host;
    /// use confitul::LocalHostOptions;
    ///
    /// let local_host = LocalHost::new(Some(LocalHostOptions::new().with_name("my-computer"))).unwrap();
    /// assert_eq!("my-computer", local_host.name());
    /// ```
    fn name(&self) -> &String {
        &self.info().name
    }

    /// Description of the host.
    ///
    /// Helper method to access directly the description of the host.
    ///
    /// # Examples
    /// ```
    /// use confitul::LocalHost;
    /// use confitul::Host;
    /// use confitul::LocalHostOptions;
    ///
    /// let local_host = LocalHost::new(Some(LocalHostOptions::new().with_description("a test"))).unwrap();
    /// assert_eq!("a test", local_host.description());
    /// ```
    fn description(&self) -> &String {
        &self.info().description
    }

    /// URLs of the host.
    ///
    /// Helper method to access directly the URLs of the host.
    ///
    /// # Examples
    /// ```
    /// use confitul::LocalHost;
    /// use confitul::Host;
    ///
    /// let local_host = LocalHost::new(None).unwrap();
    /// let info = local_host.info();
    /// assert_eq!(&info.urls, local_host.urls());
    /// ```
    fn urls(&self) -> &Vec<Url> {
        &self.info().urls
    }

    /// Signature of the host.
    ///
    /// Helper method to access directly the signature of the host.
    ///
    /// # Examples
    /// ```
    /// use confitul::LocalHost;
    /// use confitul::Host;
    ///
    /// let local_host = LocalHost::new(None).unwrap();
    /// let info = local_host.info();
    /// assert_eq!(info.sig, local_host.sig());
    /// ```
    fn sig(&self) -> HostSig {
        self.info().sig
    }

    /// Verify a message signature.
    ///
    /// Useful to check incoming messages, check they come from the
    /// right host and have been correctly signed.
    ///
    /// # Examples
    /// ```
    /// use confitul::LocalHost;
    /// use confitul::Host;
    ///
    /// let local_host = LocalHost::new(None).unwrap();
    /// let msg = "a message".as_bytes();
    /// let sig = local_host.sign_msg(msg);
    /// let msg2 = "another message".as_bytes();
    /// let sig2 = local_host.sign_msg(msg2);
    /// assert!(matches!(local_host.verify_msg(msg, &sig), Ok(())));
    /// assert!(matches!(local_host.verify_msg(msg, &sig2), Err(_)));
    /// ```
    fn verify_msg(&self, msg: &[u8], sig: &HostSig) -> Result<(), signature::Error>;

    /// Verify this host signature.
    ///
    /// This can be used when de-serializing content from external
    /// sources -> is that host legit or not ?
    ///
    /// # Examples
    /// ```
    /// use confitul::LocalHost;
    /// use confitul::Host;
    ///
    /// let local_host = LocalHost::new(None).unwrap();
    /// assert!(matches!(local_host.verify_self(), Ok(())));
    /// ```
    fn verify_self(&self) -> Result<(), signature::Error> {
        self.verify_msg(&self.info().content_to_verify(), &self.sig())
    }
}
