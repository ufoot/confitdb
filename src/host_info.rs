// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::host_id::HostId;
use crate::host_sig::HostSig;
use serde::{Deserialize, Serialize};
use std::fmt::Formatter;
use url::Url;

/// HostInfo contains the informations about a host.
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct HostInfo {
    /// ID used by the host. This happens to be the public key as well,
    /// when it comes to signing messages.
    pub id: HostId,
    /// Name of the host, a free-form test, by default the hostname.
    pub name: String,
    /// Description of the host, a free-form text.
    pub description: String,
    /// URLs of the host, how to connect to it.
    pub urls: Vec<Url>,
    /// Signature of the host info.
    pub sig: HostSig,
}

impl HostInfo {
    pub(crate) fn content_to_verify(&self) -> Vec<u8> {
        Vec::from(format!("{}", &self))
    }
}

impl std::fmt::Display for HostInfo {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{{\"id\":\"{}\",\"name\":\"{}\",\"description\":\"{}\",\"urls\":[",
            self.id, self.name, self.description,
        )?;
        let mut require_comma = false;
        for url in self.urls.iter() {
            if require_comma {
                write!(f, ",")?;
                require_comma = true;
            }
            write!(f, "\"{}\"", url.as_str())?;
        }
        write!(f, "]}}")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::host_id::HostId;
    use crate::host_sig::HostSig;
    use ed25519_dalek::Keypair;
    use rand07::rngs::OsRng;
    use url::Url;

    #[test]
    fn test_display() {
        let mut csprng = OsRng {};
        let keypair: Keypair = Keypair::generate(&mut csprng);

        let h = HostInfo {
            id: HostId::new(&keypair),
            name: String::from("computer"),
            description: String::from("test"),
            urls: vec![Url::parse("http://localhost").unwrap()],
            sig: HostSig::default(),
        };
        assert_eq!("{\"id\":\"0x", &format!("{}", h)[0..9]);
        assert_eq!(
            "\",\"name\":\"computer\",\"description\":\"test\",\"urls\":[\"http://localhost/\"]}",
            &format!("{}", h)[18..88]
        );
    }
}
