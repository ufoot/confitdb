// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::crypto::*;
use crate::error::Error;
use ed25519_dalek;
use hex::ToHex;
use serde::de;
use serde::de::Deserializer;
use serde::ser::Serializer;
use serde::{Deserialize, Serialize};
use std::convert;
use std::convert::TryFrom;
use std::fmt::Formatter;

pub const HOST_SIG_STR_LEN: usize = 9;
pub const HOST_SIG_LEN: usize = ed25519_dalek::SIGNATURE_LENGTH;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct HostSig {
    value: [u8; HOST_SIG_LEN],
}

impl HostSig {
    pub fn new(key: &ed25519_dalek::Keypair, msg: &[u8]) -> HostSig {
        HostSig::from(sign(key, msg))
    }

    pub fn bytes(&self) -> &[u8] {
        &self.value
    }
}

impl From<HostSig> for String {
    fn from(sig: HostSig) -> String {
        format!("{}", sig)
    }
}

impl TryFrom<HostSig> for ed25519_dalek::Signature {
    type Error = signature::Error;

    fn try_from(sig: HostSig) -> Result<ed25519_dalek::Signature, signature::Error> {
        ed25519_dalek::Signature::from_bytes(&sig.value)
    }
}

impl From<[u8; HOST_SIG_LEN]> for HostSig {
    fn from(buf: [u8; HOST_SIG_LEN]) -> HostSig {
        HostSig { value: buf }
    }
}

impl TryFrom<Vec<u8>> for HostSig {
    type Error = Error;

    fn try_from(vec: Vec<u8>) -> Result<HostSig, Error> {
        match <[u8; HOST_SIG_LEN]>::try_from(vec) {
            Ok(buf) => Ok(HostSig::from(buf)),
            Err(e) => Err(Error::invalid_data("invalid signaturei len")),
        }
    }
}

impl std::fmt::Display for HostSig {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut s: String = self.value.encode_hex();
        if s.len() > HOST_SIG_STR_LEN - 2 {
            s = s[..HOST_SIG_STR_LEN - 2].to_string();
        }
        write!(f, "0x{}", s)
    }
}

// https://serde.rs/impl-serialize.html
impl Serialize for HostSig {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        self.value.to_vec().serialize(serializer)
    }
}

// https://serde.rs/deserialize-struct.html
impl<'de> Deserialize<'de> for HostSig {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let vec: Vec<u8> = Vec::<u8>::deserialize(deserializer)?;
        match HostSig::try_from(vec) {
            Ok(host_sig) => Ok(host_sig),
            Err(e) => Err(de::Error::custom(&format!("{}", e))),
        }
    }
}

impl Default for HostSig {
    fn default() -> Self {
        HostSig {
            value: [0; HOST_SIG_LEN],
        }
    }
}
