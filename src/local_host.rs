// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::config::Config;
use crate::crypto::{sign, verify};
use crate::host::Host;
use crate::host_id::HostId;
use crate::host_info::HostInfo;
use crate::host_sig::HostSig;
use ed25519_dalek;
use ed25519_dalek::Keypair;
use rand07::rngs::OsRng;
use serde::{Deserialize, Serialize};
use std::fmt::Formatter;
use url::{ParseError, Url};

/// LocalHost is a host for which this instance of the program
/// is responsible.
///
/// It holds the private key for the host, which
/// should never be transmitted over the wire to any other peer.
///
/// It is serializable because you may want to re-use a host
/// after a program restart, to inform other peers that yes,
/// you are still the same host, after all.
#[derive(Debug, Serialize, Deserialize)]
pub struct LocalHost {
    info: HostInfo,
    keypair: ed25519_dalek::Keypair,
}

impl LocalHost {
    pub fn new(config: Option<Config>) -> Result<LocalHost, ParseError> {
        let real_config = config.unwrap_or(Config::default());
        let mut csprng = OsRng {};
        let keypair: Keypair = Keypair::generate(&mut csprng);
        let mut local_host = LocalHost {
            info: HostInfo {
                id: HostId::new(&keypair),
                name: real_config.get_local_host_name(),
                description: real_config.get_local_host_description(),
                urls: Vec::new(),
                sig: HostSig::default(),
            },
            keypair,
        };
        local_host.update_urls(&real_config.get_local_host_urls())?; // update_urls will update the sig
        Ok(local_host)
    }

    fn update_sig(&mut self) {
        let sig = sign(&self.keypair, &self.info.content_to_verify());
        self.info.sig = HostSig::from(sig);
    }

    pub fn update_name(&mut self, name: &str) {
        self.info.name = name.to_string();
        self.update_sig();
    }

    pub fn update_description(&mut self, description: &str) {
        self.info.description = description.to_string();
        self.update_sig();
    }

    pub fn update_urls(&mut self, urls: &Vec<String>) -> Result<(), ParseError> {
        let mut parsed_urls: Vec<Url> = Vec::new();
        for url in urls {
            let parsed_url = Url::parse(url)?;
            parsed_urls.push(parsed_url);
        }
        self.info.urls = parsed_urls;
        self.update_sig();
        Ok(())
    }
}

impl Host for LocalHost {
    fn info(&self) -> &HostInfo {
        &self.info
    }
    fn verify_msg(&self, msg: &[u8], sig: &HostSig) -> Result<(), signature::Error> {
        verify(&self.keypair.public, msg, sig.bytes())
    }
}

impl Signer for LocalHost {
    fn sign_msg(&self, msg: &[u8]) -> HostSig {
        HostSig::from(sign(&self.keypair, msg))
    }
}

impl std::fmt::Display for LocalHost {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{\"type\":\"local\",\"info\":{}}}", self.info)
    }
}

#[cfg(test)]
mod tests {
    use super::Host;
    use super::LocalHost;
    use serde_json;

    #[test]
    fn test_local_host_serde_json() {
        let host = LocalHost::new(None).unwrap();
        let serialized = serde_json::to_string(&host).unwrap();
        let deserialized: LocalHost = serde_json::from_str(&serialized).unwrap();
        assert_eq!(host.info(), deserialized.info());
        let msg = "message".as_bytes();
        let sig = deserialized.sign_msg(msg);
        assert!(matches!(host.verify_msg(msg, &sig), Ok(())));
    }

    #[test]
    fn test_sign_msg() {
        let local_host = LocalHost::new(None).unwrap();
        let msg = "a message".as_bytes();
        let sig = local_host.sign_msg(msg);
        let msg2 = "another message".as_bytes();
        let sig2 = local_host.sign_msg(msg2);
        assert!(matches!(local_host.verify_msg(msg, &sig), Ok(())));
        assert!(matches!(local_host.verify_msg(msg, &sig2), Err(_)));
    }

    #[test]
    fn test_local_host_name_update() {
        let mut local_host = LocalHost::new(None).unwrap();
        local_host.update_name("a test");
        assert_eq!("a test", local_host.info.name.as_str());
    }

    #[test]
    fn test_local_host_description_update() {
        use super::Host;
        use super::LocalHost;

        let mut local_host = LocalHost::new(None).unwrap();
        let msg = "message".as_bytes();
        let sig = local_host.sign_msg(msg);

        local_host.update_description("another test");
        assert_eq!("another test", local_host.info().description.as_str());
        assert!(
            matches!(local_host.verify_self(), Ok(())),
            "updating description should update signature as well"
        );

        assert!(
            matches!(local_host.verify_msg(msg, &sig), Ok(())),
            "description update has no impact on previous sig"
        );
    }

    #[test]
    fn test_local_host_urls_update() {
        use super::Host;
        use super::LocalHost;
        use url::Url;

        let mut local_host = LocalHost::new(None).unwrap();
        let msg = "message".as_bytes();
        let sig = local_host.sign_msg(msg);

        local_host
            .update_urls(&vec![
                String::from("https://a-location"),
                String::from("https://another-location"),
            ])
            .unwrap();
        assert_eq!(
            vec![
                Url::parse("https://a-location").unwrap(),
                Url::parse("https://another-location").unwrap()
            ],
            local_host.info().urls
        );
        assert!(
            matches!(local_host.verify_self(), Ok(())),
            "updating URLs should update signature as well"
        );

        assert!(
            matches!(local_host.verify_msg(msg, &sig), Ok(())),
            "URLs update has no impact on previous sig"
        );
    }
}
