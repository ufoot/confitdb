// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::host_id::HostId;
use serde::{Deserialize, Serialize};
use std::fmt;
use vclock::VClock64;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Versioned<V> {
    pub version: VClock64<HostId>,
    pub value: Option<V>,
}

impl<V> Versioned<V> {
    pub fn new(version: VClock64<HostId>, value: Option<V>) -> Self {
        Versioned { version, value }
    }
}

impl<V> fmt::Display for Versioned<V> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{version: {}}}", self.version)
    }
}
