// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::host_id::HostId;
use crate::host_info::HostInfo;
use crate::host_sig::HostSig;
use signature;
use url::Url;

/// Host trait shared between local and remote hosts.
pub trait Host {
    fn info(&self) -> &HostInfo;

    fn id(&self) -> HostId {
        self.info().id
    }

    fn name(&self) -> &String {
        &self.info().name
    }

    fn description(&self) -> &String {
        &self.info().description
    }

    fn urls(&self) -> &Vec<Url> {
        &self.info().urls
    }

    fn sig(&self) -> HostSig {
        self.info().sig
    }

    fn verify_msg(&self, msg: &[u8], sig: &HostSig) -> Result<(), signature::Error>;

    fn verify_self(&self) -> Result<(), signature::Error> {
        self.verify_msg(&self.info().content_to_verify(), &self.sig())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::local_host::LocalHost;

    #[test]
    fn test_sig() {
        let local_host = LocalHost::new(None).unwrap();
        let info = local_host.info();
        assert_eq!(info.sig, local_host.sig());
    }

    #[test]
    fn test_verify_msg() {
        let local_host = LocalHost::new(None).unwrap();
        let msg = "a message".as_bytes();
        let sig = local_host.sign_msg(msg);
        let msg2 = "another message".as_bytes();
        let sig2 = local_host.sign_msg(msg2);
        assert!(matches!(local_host.verify_msg(msg, &sig), Ok(())));
        assert!(matches!(local_host.verify_msg(msg, &sig2), Err(_)));
    }

    #[test]
    fn test_verify_self() {
        let local_host = LocalHost::new(None).unwrap();
        assert!(matches!(local_host.verify_self(), Ok(())));
    }
}
