// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::error::Error;
use ed25519_dalek;
use hex::ToHex;
use serde::{Deserialize, Serialize};
use std::convert;
use std::convert::TryFrom;
use std::fmt::Formatter;

pub const HOST_ID_STR_LEN: usize = 11;
pub const HOST_ID_LEN: usize = ed25519_dalek::PUBLIC_KEY_LENGTH;

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct HostId {
    value: [u8; HOST_ID_LEN],
}

impl HostId {
    pub fn new(key: &ed25519_dalek::Keypair) -> HostId {
        HostId::from(key.public.to_bytes())
    }

    pub fn bytes(&self) -> &[u8] {
        &self.value
    }
}

impl From<HostId> for String {
    fn from(id: HostId) -> String {
        format!("{}", id)
    }
}

impl TryFrom<HostId> for ed25519_dalek::PublicKey {
    type Error = signature::Error;

    fn try_from(id: HostId) -> Result<ed25519_dalek::PublicKey, signature::Error> {
        ed25519_dalek::PublicKey::from_bytes(&id.value)
    }
}

impl From<[u8; HOST_ID_LEN]> for HostId {
    fn from(buf: [u8; HOST_ID_LEN]) -> HostId {
        HostId { value: buf }
    }
}

impl TryFrom<Vec<u8>> for HostId {
    type Error = Error;

    fn try_from(vec: Vec<u8>) -> Result<HostId, Error> {
        match <[u8; HOST_ID_LEN]>::try_from(vec) {
            Ok(buf) => Ok(HostId::from(buf)),
            Err(e) => Err(Error::invalid_data("invalid ID len")),
        }
    }
}

impl std::fmt::Display for HostId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut s: String = self.value.encode_hex();
        if s.len() > HOST_ID_STR_LEN - 2 {
            s = s[..HOST_ID_STR_LEN - 2].to_string();
        }
        write!(f, "0x{}", s)
    }
}
