// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

//! [ConfitDB](https://gitlab.com/liberecofr/confitdb) is an experimental,
//! distributed, real-time database, giving full control on
//! conflict resolution, implemented in [Rust](https://www.rust-lang.org/).
//!
//! ![ConfitDB icon](https://gitlab.com/liberecofr/confitdb/raw/main/media/confitdb-256x256.jpg)
//!
//! Not even a prototype, just an idea. Under heavy construction.

mod config;
mod constant;
mod crypto;
mod db;
mod entry;
mod error;
mod host;
mod host_id;
mod host_info;
mod host_sig;
mod local_host;
mod message;
mod raw_message;
mod signer;
mod versioned;

pub use config::Config;
pub use db::DB;
pub use error::{Error, Result};
pub use host::Host;
pub use host_id::HostId;
pub use host_info::HostInfo;
pub use host_sig::HostSig;
