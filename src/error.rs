// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use std::fmt;
use std::fmt::{Display, Formatter};

/// Error type used by ConfigDB.
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Error {
    /// Configuration related error.
    Config(String),
    /// Network related error.
    Network(String),
    /// Storage related error.
    Storage(String),
    /// Invalid data, typically a serialization problem, or invalid input.
    InvalidData(String),
    /// Unexpected code path are behavior, please report issue on
    /// <https://gitlab.com/liberecofr/confitdb/-/issues>.
    ReportBug(String),
    /// Feature is not implemented yet.
    NotImplemented,
}

/// URL to report bugs.
///
/// This is used internally to provide context when something unexpected happens,
/// so that users can find find out which piece of software fails,
/// and how to contact author(s).
///
/// Alternatively, send a direct email to <ufoot@ufoot.org>.
pub const BUG_REPORT_URL: &str = "https://gitlab.com/liberecofr/confitdb/-/issues";

/// Result type used by ConfitDB.
pub type Result<T> = std::result::Result<T, Error>;

impl Error {
    pub(crate) fn config(why: &str) -> Error {
        Error::Config(why.to_string())
    }

    pub(crate) fn network(why: &str) -> Error {
        Error::Network(why.to_string())
    }

    pub(crate) fn storage(why: &str) -> Error {
        Error::Storage(why.to_string())
    }

    pub(crate) fn invalid_data(why: &str) -> Error {
        Error::InvalidData(why.to_string())
    }

    pub(crate) fn report_bug(why: &str) -> Error {
        Error::ReportBug(why.to_string())
    }

    pub(crate) fn not_implemented() -> Error {
        Error::NotImplemented
    }
}

/// Convert from a MenhirKV error.
impl From<menhirkv::Error> for Error {
    fn from(e: menhirkv::Error) -> Error {
        Error::storage(&format!("[menhirkv] {}", e))
    }
}

/// Convert from unit.
///
/// This returns "not implemented", it is just here to
/// make sure all the chain serving the "not implemented"
/// series of errors is here, and public API remains
/// stable.
impl From<()> for Error {
    fn from(_: ()) -> Error {
        Error::not_implemented()
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::result::Result<(), fmt::Error> {
        match self {
            Error::Config(e) => write!(f, "config error: {}", e),
            Error::Network(e) => write!(f, "network error: {}", e),
            Error::Storage(e) => write!(f, "storage error: {}", e),
            Error::InvalidData(e) => write!(f, "invalid data: {}", e),
            Error::ReportBug(e) => write!(
                f,
                "unexpected bug, please report issue on <{}>: {}",
                BUG_REPORT_URL, e
            ),
            Error::NotImplemented => write!(f, "not implemented"),
        }
    }
}
